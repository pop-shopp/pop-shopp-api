const Product =require("../models/Product");

module.exports.getAllActiveProducts = (req,res)=>{

	Product.find({isActive:true})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))

};

module.exports.createProduct = (req,res)=>{
	
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})
	
	newProduct.save()
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
};

module.exports.getSingleProduct = (req,res)=>{

	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.updateProductInfo = (req,res) =>{

	let updatedinfo = {
		name:req.body.name,
		description:req.body.description,
		price:req.body.price
	}
	Product.findByIdAndUpdate(req.params.id,updatedinfo,{new:true})
	.then(result=> res.send(result))
	.catch(error=> res.send(error))
};

module.exports.archiveProduct = (req,res) => {


	let update = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.id,update,{new:true})
	.then(result=>res.send(result))
	.catch(error=> res.send(error))
};



module.exports.unarchiveProduct = (req,res) => {


	let update = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.id,update,{new:true})
	.then(result=>res.send(result))
	.catch(error=> res.send(error))
};

module.exports.getAllInactiveProducts = (req,res)=>{

	Product.find({isActive:false})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))

};



module.exports.findProductsByName = (req, res) => {

	Product.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if(result.length === 0){
			return res.send (false)
		} else {

			return res.send(result)
		}
	})
}

module.exports.getAllProducts = (req,res)=>{

	Product.find({})
	.then(result=>res.send(result))
	.catch(error=>res.send(error))
};